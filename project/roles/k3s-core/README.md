# Ansible k3s-core installation
A short description of the role.

## How to use
### Prerequisites
* First you need.
* This is required.

### Variables
| Variable      | Default Value   | Description                 |
| -------- | ------------- | ----------- |
| `my_variable` | `default value` | This is an example variable |

## Testing
### Local
Description of the local test.

#### Prerequisites
* ...
* ...

#### Running the test
Run this command: `command`

### Some special test
...

## Notes
### Supported platforms
| Platform  | Version |
| --------- | ------- |
| centos    | 7       |