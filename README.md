# k3s-baremetal

Exploratory project to improve kubernetes knownledge

## Status
k3s server (pi4) and k3s agent totoro (laptop-1) deployed manually as a base environment for the lab.

## Architecture
Run kubernetes on 2 old laptops and a Raspberry 4

### Deployment
Ansible playbooks deployed with ansible-runner
## Test infrastructure
Vagrant vm + 3 lxd container

